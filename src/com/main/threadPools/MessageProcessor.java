package com.main.threadPools;

public class MessageProcessor implements Runnable {
    private int message;

    public MessageProcessor(int message) {
        this.message = message;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + " [RECEIVED] Message: " + message);
        respondToMessage(); // make the thread sleep to simulate doing some work
        System.out.println(Thread.currentThread().getName() + " (Done) Processing message: " + message);
    }

    private void respondToMessage() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
